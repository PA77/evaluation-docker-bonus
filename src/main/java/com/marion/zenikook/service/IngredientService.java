package com.marion.zenikook.service;

import com.marion.zenikook.controllers.representation.ingredient.IngredientRepresentation;
import com.marion.zenikook.domain.Ingredient;
import com.marion.zenikook.repositories.IngredientRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class IngredientService {

    private IngredientRepository ingredientRepository;
    private IdGenerator idGenerator;

    public IngredientService(IngredientRepository ingredientRepository, IdGenerator idGenerator) {
        this.ingredientRepository = ingredientRepository;
        this.idGenerator = idGenerator;
    }
    public List<Ingredient> getAllIngredients() {
        return (List<Ingredient>) this.ingredientRepository.findAll();
    }

    public Optional<Ingredient> getOneIngredient(String id) {
        return this.ingredientRepository.findById(id);
    }

    public Ingredient createOneIngredient (IngredientRepresentation body) {
        Ingredient ingredient = new Ingredient(this.idGenerator.generateNewId(), body.getName());
        return this.ingredientRepository.save(ingredient);
    }

    public void deleteOneIngredient(String id) {
        this.ingredientRepository.deleteById(id);
    }

}
