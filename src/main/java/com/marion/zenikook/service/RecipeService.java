package com.marion.zenikook.service;

import com.marion.zenikook.controllers.representation.recipe.NewRecipeRepresenation;
import com.marion.zenikook.controllers.representation.recipe.RecipeRepresentation;
import com.marion.zenikook.domain.Recipe;
import com.marion.zenikook.repositories.RecipeRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class RecipeService {

        private RecipeRepository recipeRepository;
        private IdGenerator idGenerator;

    public RecipeService(RecipeRepository recipeRepository, IdGenerator idGenerator) {
        this.recipeRepository = recipeRepository;
        this.idGenerator = idGenerator;
    }

    //tenter un foreach pour empecher le cast // itérer sur chaque ligne du tableau
    public List<Recipe> getAllQRecipe() {
        return (List<Recipe>) this.recipeRepository.findAll();
    }

    public Optional<Recipe> getOneRecipe(String id) {
        return this.recipeRepository.findById(id);
    }

    public void deleteOneRecipe(String id) {
       this.recipeRepository.deleteById(id);
    }

    public Recipe createOneRecipe (NewRecipeRepresenation body) {
        Recipe recipe =new Recipe(this.idGenerator.generateNewId(), body.getName(), body.getRecipeContent());
        return this.recipeRepository.save(recipe);
    }

    public Recipe modifyOneRecipe (String id, RecipeRepresentation body) {
        // A FAIRE : ne pas sortir de l'optional avec
        Recipe recipeToModify = recipeRepository.findById(id).get();
        Recipe recipeInBase = new Recipe(id, body.getName(), body.getRecipeContent());
        String newRecipeContent = "";
        String newName = "";
        if (recipeInBase.getName().equals("")) {
            newName = recipeToModify.getName();
        } else {
            newName = recipeInBase.getName();
        }
        if (recipeInBase.getRecipeContent().equals("")) {
            newRecipeContent = recipeToModify.getRecipeContent();
        } else {
            newRecipeContent = recipeInBase.getRecipeContent();
        }
        Recipe newRecipe = new Recipe(id,newName, newRecipeContent);
        return this.recipeRepository.save(newRecipe);

    }
}

/*
    Optional<Recipe> toModify = this.recipeRepository.findById(id);
    if (toModify.isPresent()) {
        if (toModify.get().getName() != body.getName() && body.getName() != null) {
            toModify.get().setName(body.getName());
        }
        if (toModify.get().getRecipeContent() != body.getRecipe_content() && body.getRecipe_content() != null) {
            toModify.get().setRecipeContent(body.getRecipe_content());
        }
    }
    this.recipeRepository.save(toModify.get());
}
 */