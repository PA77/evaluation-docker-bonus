package com.marion.zenikook.domain;


import javax.persistence.*;

@Entity
@Table(name="recipes")
public class Recipe {

    @Id
    private String id;
    private String name;
    private String recipeContent;


    public Recipe(String id, String name, String recipeContent) {
        this.id = id;
        this.name = name;
        this.recipeContent = recipeContent;
    }

    protected Recipe() {}

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getRecipeContent() {
        return recipeContent;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRecipeContent(String recipeContent) {
        this.recipeContent = recipeContent;
    }
}
