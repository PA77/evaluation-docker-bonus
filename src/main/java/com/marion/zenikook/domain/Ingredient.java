package com.marion.zenikook.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class Ingredient {

    @Id
    private String id;
    private String name;

    public Ingredient (String id, String name ) {
        this.id = id;
        this.name = name;
    }
     protected Ingredient () {
             }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
