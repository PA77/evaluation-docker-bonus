package com.marion.zenikook.controllers.representation.ingredient;

import com.marion.zenikook.domain.Ingredient;
import org.springframework.stereotype.Component;

@Component
public class IngredientRepresentationMapper {
    public IngredientRepresentation mapToDisplayableIngredient(Ingredient ingredient) {
        IngredientRepresentation result = new IngredientRepresentation();
        result.setId(ingredient.getId());
        result.setName(ingredient.getName());
        return result;
    }
}
