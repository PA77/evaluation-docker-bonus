package com.marion.zenikook.controllers;

import com.marion.zenikook.controllers.representation.recipe.NewRecipeRepresenation;
import com.marion.zenikook.controllers.representation.recipe.RecipeRepresentation;
import com.marion.zenikook.controllers.representation.recipe.RecipeRepresentationMapper;
import com.marion.zenikook.domain.Recipe;
import com.marion.zenikook.service.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/recipes")
public class RecipeController {

    private RecipeService recipeService;
    private RecipeRepresentationMapper recipeRepresentationMapper;

    @Autowired
    public RecipeController(RecipeService recipeService, RecipeRepresentationMapper recipeRepresentationMapper) {
        this.recipeService = recipeService;
        this.recipeRepresentationMapper = recipeRepresentationMapper;
    }

    @GetMapping
    List<RecipeRepresentation> getAllRecipe() {
        return this.recipeService.getAllQRecipe().stream()
                .map(this.recipeRepresentationMapper::mapToDisplayableRecipe)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    ResponseEntity<RecipeRepresentation> getOneRecipe(@PathVariable("id") String id) {
        Optional<Recipe> recipe = this.recipeService.getOneRecipe(id);
        return recipe
                .map(this.recipeRepresentationMapper::mapToDisplayableRecipe)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<RecipeRepresentation> deleteOneRecipe(@PathVariable("id") String id) {
       this.recipeService.deleteOneRecipe(id);
       return ResponseEntity.noContent().build();
    }

    @PostMapping
    public RecipeRepresentation createRecipe(@RequestBody NewRecipeRepresenation body) {
        return recipeRepresentationMapper.mapToDisplayableRecipe(this.recipeService.createOneRecipe(body));
    }

    @PutMapping("/{id}")
    public ResponseEntity<RecipeRepresentation> modifyQuestion(@PathVariable ("id") String id ,
                                                  @RequestBody RecipeRepresentation body) {
        this.recipeService.modifyOneRecipe(id, body);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
