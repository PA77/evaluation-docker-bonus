package com.marion.zenikook.controllers;

import com.marion.zenikook.controllers.representation.ingredient.IngredientRepresentation;
import com.marion.zenikook.controllers.representation.ingredient.IngredientRepresentationMapper;
import com.marion.zenikook.controllers.representation.recipe.NewRecipeRepresenation;
import com.marion.zenikook.controllers.representation.recipe.RecipeRepresentation;
import com.marion.zenikook.domain.Ingredient;
import com.marion.zenikook.domain.Recipe;
import com.marion.zenikook.service.IngredientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/ingredient")
public class IngredientController {

    private IngredientService ingredientService;
    private IngredientRepresentationMapper ingredientRepresentationMapper;

    @Autowired
    public IngredientController(IngredientService ingredientService, IngredientRepresentationMapper ingredientRepresentationMapper) {
        this.ingredientService = ingredientService;
        this.ingredientRepresentationMapper = ingredientRepresentationMapper;
    }
    @GetMapping
    List<IngredientRepresentation> getAllIngredient() {
        return this.ingredientService.getAllIngredients().stream()
                .map(this.ingredientRepresentationMapper::mapToDisplayableIngredient)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    ResponseEntity<IngredientRepresentation> getOneIngredient(@PathVariable("id") String id) {
        Optional<Ingredient> ingredient = this.ingredientService.getOneIngredient(id);
        return ingredient
                .map(this.ingredientRepresentationMapper::mapToDisplayableIngredient)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
    @PostMapping
    public IngredientRepresentation createIngredient (@RequestBody IngredientRepresentation body) {
        return ingredientRepresentationMapper.mapToDisplayableIngredient(this.ingredientService.createOneIngredient(body));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<IngredientRepresentation> deleteOneIngredient(@PathVariable("id") String id) {
        this.ingredientService.deleteOneIngredient(id);
        return ResponseEntity.noContent().build();
    }

}
