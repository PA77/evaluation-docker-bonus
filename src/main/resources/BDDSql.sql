create table recipes
(
id text primary key,
name text not null,
recipe_content text not null
)

insert into recipes
(id, name, recipe_content)
values ('4d383fac-0941-4f36-967a-fedbf07fefcb', 'galettes complètes', 'galette, jambon, oeuf, fromage');

DELETE FROM recipes
WHERE recipes.id = '4d383fac-0941-4f36-967a-fedbf07fefcb'

insert into recipes (id, name, recipe_content) values (
	'e806a02b-1f30-4a3c-8bbf-1c1758c564cb',
	'Lemon cheesecake',
	'creamCheese, speculos, oeuf, lait, sucre');

insert into recipes
values ('c546693e-9952-4ae1-a858-f98415ed455b', 'oeuf à la mexicaine', 'oeuf, tomate, piments verts, oignon, sel');

create table ingredient
(
id text primary key,
name text not null
)

insert into ingredient (id, name)
values
('1', 'galette de blé noir'),
('2', 'jambon'),
('3', 'oeuf'),
('4', 'fromage')
('5', 'creamCheese'),
('6', 'speculos'),
('7', 'lait'),
('8', 'sucre'),
('9', 'tomate'),
('10', 'piments verts'),
('11', 'oignon'),
('12', 'sel')

create table composition
(
id_ingredient text NOT NULL ,
id_recipe text NOT NULL ,
unit text not null,
quantity int not null
PRIMARY KEY( id_recipe, id_ingredient ),
    CONSTRAINT fk_recette_composition
        FOREIGN KEY id_recette
        REFERENCES recette ( id_recette ),

)
( id_recette [pk][fk], id_ingredient [pk][fk], quantite, ordre )
composition (
    id_recipe TEXT UNSIGNED NOT NULL,
    id_ingredient TEXT UNSIGNED NOT NULL,
    PRIMARY KEY( id_recette, id_ingredient, ordre ),
    KEY ( id_recette ),
    CONSTRAINT fk_recette_composition
        FOREIGN KEY id_recette
        REFERENCES recette ( id_recette ),
    KEY ( id_ingredient ),
    CONSTRAINT fk_ingredient_composition
        FOREIGN KEY id_recette
        REFERENCES ingredient ( id_ingredient )
)

create table measures
(
   id_ingredient  text references ingredients (id),
   id_recipe  text references recipes (id),
   unit      text not null,
   quantity   int not null,
   PRIMARY KEY (id_ingredient, id_recipe)
);



create table steps
(
id text PRIMARY KEY,
order_step int NOT null,
id_recipe text REFERENCES recipes (id),
description text not null
)

insert into measures (id_ingredient, id_recipe, unit, quantity)
values
('1', '4d383fac-0941-4f36-967a-fedbf07fefcb', 'unité', 1),
('2', '4d383fac-0941-4f36-967a-fedbf07fefcb', 'grammes', 30),
('3', '4d383fac-0941-4f36-967a-fedbf07fefcb', 'unité', 1),
('4', '4d383fac-0941-4f36-967a-fedbf07fefcb', 'grammes', 10),
('5', 'e806a02b-1f30-4a3c-8bbf-1c1758c564cb', 'grammes', 50),
('6', 'e806a02b-1f30-4a3c-8bbf-1c1758c564cb', 'unité', 5),
('7', 'e806a02b-1f30-4a3c-8bbf-1c1758c564cb', 'centilitres', 20),
('8', 'e806a02b-1f30-4a3c-8bbf-1c1758c564cb', 'grammes', 30),
('3', 'e806a02b-1f30-4a3c-8bbf-1c1758c564cb', 'unité', 2),
('3', '4d383fac-0941-4f36-967a-fedbf07fefcb', 'unité', 2),
('9', '4d383fac-0941-4f36-967a-fedbf07fefcb', 'unité', 2),
('10', '4d383fac-0941-4f36-967a-fedbf07fefcb', 'unité', 1),
('11', '4d383fac-0941-4f36-967a-fedbf07fefcb', 'unité', 1),
('12', '4d383fac-0941-4f36-967a-fedbf07fefcb', 'pincée', 1)

ALTER TABLE measures
ADD PRIMARY KEY (id_ingredient, id_recipe)

alter table measures rename to composition

